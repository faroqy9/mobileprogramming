// NO 1
const golden = goldenFunction = () => {
     console.log("this is golden!!");
}

golden();

// NO 2
const newFunction = literal = (firstName, lastName) => {
     return {
          name: {firstName, lastName},
          fullName: function(){ 
               console.log(this.name.firstName + " " + this.name.lastName);
          }      
     } 
} 
//Driver Code
newFunction("William", "Imoh").fullName()

// NO 3
const newObject = { 
     firstName: "Harry", 
     lastName: "Potter Holt", 
     destination: "Hogwarts React Conf", 
     occupation: "Deve-wizard Avocado", 
     spell: "Vimulus Renderus!!!" 
}

const {firstName, lastName, destination, occupation, spell} = newObject;

console.log(firstName, lastName, destination, occupation);

// NO 4

const west = ["Will", "Chris", "Sam", "Holly"] 
const east = ["Gill", "Brian", "Noel", "Maggie"] 
let combined = [...west, ...east];
//Driver Code 
console.log(combined);

// NO 5

const planet = "earth" 
const view = "glass" 
let before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(before)